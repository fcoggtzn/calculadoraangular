FROM nginx:alpine
COPY /dist/frontendcal /usr/share/nginx/html
COPY ./default.conf /etc/nginx/conf.d/default.conf
