import { TestBed } from '@angular/core/testing';

import { CalculadoraService } from './calculadora.service';
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('CalculadoraService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule]
  }));

  it('should be created', () => {
    const service: CalculadoraService = TestBed.get(CalculadoraService);
    expect(service).toBeTruthy();
  });
});
