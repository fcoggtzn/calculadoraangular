import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService {

  constructor(private http: HttpClient) { }

  suma(x: number, y:number){
    return this.http.get<String>("http://localhost:8080/cal/?x="+x+"&y="+y,{observe: 'response'}); 
  }
}
