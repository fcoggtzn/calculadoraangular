import { Component, OnInit } from '@angular/core';
import { CalculadoraService } from '../service/calculadora.service';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.scss']
})
export class CalculadoraComponent implements OnInit {
  x  : number = 5;
  y  : number = 5;
  res : number = 0;
  
  constructor(public api: CalculadoraService ){
   
   
  };
  
  sumar(){
  this.api.suma(this.x,this.y).subscribe((data: any) => {
    console.log (data.body);
     this.res = data.body;
  //   alert(this.res);
  });
}

  ngOnInit() {
  }

}
